grammar IntOrSTMEND;

INT :('0'..'9')+;
fragment SEMICOLON: ';';
NEWLINE: ('\r\n'|'\n'|'\r');
STMTEND: (SEMICOLON (NEWLINE)*|NEWLINE+);

statement: (num STMTEND)+;
num: 'INT ' INT;
