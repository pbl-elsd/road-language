package src.base;

import src.base.common.App;
import src.base.utils.RoadConstructionTestParser;

public class Main {

    public static void main(String[] args) {
        App app = new App();
        /*System.out.println(new RoadConstructionTestParser("ROAD Stefan;\nROAD Luca;").parseDeclareRoadVar());
        System.out.println(new RoadConstructionTestParser("ROAD Stefan;\nROAD Caragiale;\nINTERSECTION Stefan-Caragiale;\nSetWidth(Stefan, 500);").parse());*/
        System.out.println(new RoadConstructionTestParser("ROAD Stefan;\n" +
                "ROAD Caragiale;\n" +
                "\n" +
                "SIGN sign(Caragiale, giveWay, 95, left);\n"+
                "\n" +
                "ROAD Stefan;\n" +
                "\n" +
                "SetRoadWidth(Stefan, 18);\n" +
                "SetPriority(Stefan, Stefan-Caragiale, Main);\n" +
                "SetMovementType(Stefan, Bilateral);\n" +
                "SetLongtitudinalMarking(Stefan, DoubleSolid);\n" +
                "\n" +
                "SetRoadWidth(Caragiale, 8);\n" +
                "SetRoadLength(Caragiale, 10);\n" +
                "SetPriority(Caragiale, Stefan-Caragiale, GiveWay);\n" +
                "SetMovementType(Caragiale, Unilateral);\n" +
                "SetLongtitudinalMarking(Caragiale, Broken);\n" +
                "\n" +
                "INTERSECTION Stefan-Caragiale;\n" +
                "SetIntersectionType(Stefan-Caragiale, Regulated);\n" +
                "SetCrossingAngle(Caragiale, 90);\n" +
                "\n" +
                "AutoMarkup();\n" +
                "Draw();\n", app).parse());
        /*System.out.println(new RoadConstructionTestParser("SetWidth(Caragiale, 8);").parseSetWidth());
        System.out.println(new RoadConstructionTestParser("RROAD a;").parse());*/
    }



}
