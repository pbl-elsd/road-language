package src.base.RoadConstruction;

import src.base.common.EventParser;

import java.util.ArrayList;

public class Intersection {
    private String varName;

    private ArrayList<Road> roads;
    private int length = 20;
    private int width = 20;

    public Intersection(String input) {
        this.varName = EventParser.getInputArguments(input)[0];
    }

    public ArrayList<Road> getRoads() {
        return roads;
    }

    public void setRoad(int index, Road road) {roads.set(index, road);}

    public void setRoads(ArrayList<Road> roads) {
        this.roads = roads;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getVarName(){
        return varName;
    }
}
