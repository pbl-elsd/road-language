package src.base.RoadConstruction;

import src.base.common.EventParser;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Road {
    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    private String varName;

    private ArrayList<Sign> signs = new ArrayList<>();

    private int width = 10;
    private int length = 200;
    private int angle = 0;

    public Road(){
    }

    public Road(String input){
        this.varName = EventParser.getInputArguments(input)[0];
    }

    public Road(int width){
        this.width = width;
    }

    public ArrayList<Sign> getSigns() {return this.signs;}

    public void setSign (int index, Sign sign) {this.signs.set(index, sign);}

    public void setSigns (ArrayList<Sign> signs) {this.signs = signs;}

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth(){
        return this.width;
    }

    public void setWidth(int width){
        this.width = width;
    }

    public String getVarName(){
        return varName;
    }
	
	@Override
    public String toString(){
        return this.varName + " " + this.getLength() + " " + this.getWidth() + " " + this.getAngle();
    }
}
