package src.base.RoadConstruction;

import src.base.common.EventParser;

import java.util.HashMap;
import java.util.StringJoiner;

public class Sign {

    private final String varName;

    private Road road;
    //what road belongs it.

    private int offset;
    //distance from the beginning of the road

    private String sideOfRoad;
    //on which side of the road it is located

    private String type;
    //type of sign itself

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getSideOfRoad() {
        return sideOfRoad;
    }

    public Sign(String input, HashMap<String, Road> roadHashMap, HashMap<String, Sign> signHashMap) {
        System.out.println("QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ " + EventParser.getInputArguments(input)[0]);
        String roadName = EventParser.getInputArguments(input)[0];
        Road road = roadHashMap.get(roadName);

        this.varName = EventParser.getVarNameFromConstructorExpression(input);
        this.road = road;
        this.type = EventParser.getInputArguments(input)[1];
        this.offset = Integer.parseInt(EventParser.getInputArguments(input)[2]);
        this.sideOfRoad =EventParser.getInputArguments(input)[3];

        signHashMap.put(varName, this);

        System.out.println("aaaaaaaaa");
        System.out.println(this);
    }

    public void setRoad(Road road) {
        this.road = road;
    }

    public void setDistance(int distance) {
        this.offset = distance;
    }

    public void setSideOfRoad(String sideOfRoad) {
       /* if (sideOfRoad.equalsIgnoreCase("left")) this.sideOfRoad = Side.LEFT;
        if (sideOfRoad.equalsIgnoreCase("right")) this.sideOfRoad = Side.RIGHT;
        //there have to be an error about wrong side.*/

        this.sideOfRoad = sideOfRoad.equals("left") ? "left" : "right";
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Sign.class.getSimpleName() + "[", "]")
                .add("varName='" + varName + "'")
                .add("road=" + road)
                .add("offset=" + offset)
                .add("sideOfRoad='" + sideOfRoad + "'")
                .add("type='" + type + "'")
                .toString();
    }

    public void setType(String type) {
        /*for (SignType enumType: SignType.values())
            if (enumType.name().equalsIgnoreCase(type))
            { this.type = enumType; return; }
        //there have to be an error about wrong sign type.*/

        this.type = type;
    }

    public String getVarName() {
        return varName;
    }

    public String getType() {
        return type;
    }

    public Road getRoad() {
        return road;
    }
}
