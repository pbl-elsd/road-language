package src.base.common;

import src.base.RoadConstruction.Intersection;
import src.base.RoadConstruction.Road;
import src.base.RoadConstruction.Sign;

import java.util.ArrayList;
import java.util.HashMap;

public class App {
    public HashMap<String, Object> appHashMap = new HashMap<>();
    public ArrayList<Object> appArrayList = new ArrayList<>();

    public HashMap<String, Road> roadHashMap = new HashMap<>();
    public HashMap<String, Intersection> intersectionHashMap = new HashMap<>();
    public HashMap<String, Sign> signHashMap = new HashMap<>();

    public App() {
    }

}
