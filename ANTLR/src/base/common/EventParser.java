package src.base.common;

import src.base.common.Constants.*;

import static src.base.common.Constants.*;

public class EventParser {

    //this class is created to make easier assignment of the values to objects' attributes (arguments).
    //so we don't need to write input parsing for every object and function, but to make it universal.

    private static final String regCreateElement ="[A-Z]+ ([A-Z]|[a-z]|[0-9])+";
    //our DSL functions of creating element has the following shape - for example creation of the road "ROAD %name%",
    //so the first word is uppercase type of the object is has to create ( ---> [A-Z]+ <--- ), and then the name of
    //the object (it can contain uppercase/lowercase letters and digits) ( ---> ([A-Z]|[a-z]|[0-9])+ <--- ).

    private static final String regFunctionWArg="[A-Z]([A-Z]|[a-z])+[(](/d, |[A-Za-z0-9-]+, |[A-Za-z0-9]+, )*(/d|[A-Za-z0-9-]+|[A-Za-z0-9]+)[)]";
    //our DSL functions of setting values to attributes of the object has the following shape - for example setting
    // length of the road is "SetLength(%name%, %value%)", so the first part is the name of the function - it starts
    // with uppercase character, and then there can be any uppercase/lowercase letter ( ---> [A-Z]([A-Z]|[a-z])+ <---).
    // After than there is "(%argument1%, ...,  %argumentN%)" so the regexp searches for arguments and does not depend
    // on the number of arguments.
    // The regex defines last argument ( ---> (/d|[A-Za-z0-9-]+|[A-Za-z0-9]+) <--- )
    // and the other arguments ( ---> (/d, |[A-Za-z0-9-]+, |[A-Za-z0-9]+, )* <--- ).
    // For example, if there is only one argument, it means that it is the last one and there is no other arguments.

    // SIGN s1(stefan, giveWay, 150, left)
    private static final String regConstructor="[A-Z]+ ([A-Z]|[a-z])+([0-9])*[(](([A-Z]|[a-z]|[0-9])+[,][ ]*)*([A-Z]|[a-z]|[0-9])+[ ]*[)]";


    public static String[] getInputArguments(String input) {
        input = input.trim();
        if (input.matches(regCreateElement)) { return new String[]{getSingleInputArgument(input)};} else
        if (input.matches(regFunctionWArg)) {
            System.out.println(ANSI_BLUE + "IN regFunctionWArg!" + ANSI_RESET + input);
            return getSplittedInputArguments(input); }
        if (input.matches(regConstructor)) {
            System.out.println(ANSI_BLUE + "IN regConstructor!" + ANSI_RESET);
            int bracketIndex = input.indexOf('(');
            String part1 = input.substring(0, bracketIndex);
            String part2 = input.substring(bracketIndex + 1, input.length() - 1);
            String varName = getSingleInputArgument(part1);
            String[] args = getConstructorArguments(part2);

            System.out.println(part1 + "\n" + part2 + "\n" + varName + "\n");
            System.out.println("regConstructor ARGS: ");

            for (int i = 0; i < args.length; i++) {
                System.out.println("CONSTRUCTOR ARG: " + args[i]);
            }

            return args;
        }

        //here have to be error because of input string does not match both regexes (?)
        System.err.println("DIDNT MATCH ANY EXPR!");
        System.out.println(ANSI_GREEN + input + ANSI_RESET);
        return new String[]{""};
    }

    //There are three types of the functions in our DSL:

    //createElement
    private static String getSingleInputArgument(String input) {
        return input.substring(input.indexOf(' ')+1);
    }

    //function with arguments
    private static String[] getSplittedInputArguments(String input) {
            input = input.substring(input.indexOf('(')+1,input.length()-1);
            return input.split(", *");
    }

    private static String[] getConstructorArguments(String input) {
        input = input.replaceAll(" ", "");
        System.out.println(ANSI_YELLOW + input + ANSI_RESET);
        /*String args[] = input.split(",");
        for(int i = 0; i < args.length; i++){
            System.out.println("CONSTRUCTOR ARG: "   + args[i]);
        }*/
        return input.split(",");
    }

    public static String getVarNameFromConstructorExpression(String input) {
        String expr = input.substring(0, input.indexOf('('));
        String varName = getSingleInputArgument(expr);

        return varName;
    }

    //function without arguments (there is no input)

}
