package src.base.utils;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import src.ANTLR.RoadConstructionParser;
import src.base.RoadConstruction.Intersection;
import src.base.RoadConstruction.Road;
import src.base.RoadConstruction.Sign;
import src.base.common.App;
import src.base.common.EventParser;
import src.base.common.TokenInputParser;
import static src.base.common.Constants.*;

import java.util.ArrayList;
import java.util.HashMap;

public class RoadConstructionListener implements src.ANTLR.RoadConstructionListener{
    public HashMap<String, Object> appHashMap = new HashMap<>();
    public ArrayList<Object> appArrayList = new ArrayList<>();
    public HashMap<String, Road> roadHashMap = new HashMap<>();
    public HashMap<String, Intersection> intersectionHashMap = new HashMap<>();
    public HashMap<String, Sign> signHashMap = new HashMap<>();

    public TokenInputParser tokenInputParser = new TokenInputParser();

    public RoadConstructionListener(){}
    public RoadConstructionListener(App app) {
        appArrayList = app.appArrayList;
        appHashMap = app.appHashMap;
        roadHashMap = app.roadHashMap;
        intersectionHashMap = app.intersectionHashMap;
        signHashMap = app.signHashMap;
    }

    @Override
    public void enterProgram(RoadConstructionParser.ProgramContext ctx) {
        System.out.println(ANSI_PURPLE + "enterProgram invoked!" + ANSI_RESET);
    }

    @Override
    public void exitProgram(RoadConstructionParser.ProgramContext ctx) {
        System.out.println(ANSI_PURPLE + "exitProgram invoked!" + ANSI_RESET);
    }

    @Override
    public void enterStatement(RoadConstructionParser.StatementContext ctx) {
        System.out.println(ANSI_PURPLE + "enterStatement invoked!" + ANSI_RESET);
    }

    @Override
    public void exitStatement(RoadConstructionParser.StatementContext ctx) {
        System.out.println(ANSI_PURPLE + "exitStatement invoked!" + ANSI_RESET);
    }

    @Override
    public void enterCreateElement(RoadConstructionParser.CreateElementContext ctx) {

    }

    @Override
    public void exitCreateElement(RoadConstructionParser.CreateElementContext ctx) {

    }

    @Override
    public void enterInvokeProcedure(RoadConstructionParser.InvokeProcedureContext ctx) {

    }

    @Override
    public void exitInvokeProcedure(RoadConstructionParser.InvokeProcedureContext ctx) {

    }

    @Override
    public void enterFinalize(RoadConstructionParser.FinalizeContext ctx) {

    }

    @Override
    public void exitFinalize(RoadConstructionParser.FinalizeContext ctx) {

    }

    @Override
    public void enterDraw(RoadConstructionParser.DrawContext ctx) {

    }

    @Override
    public void exitDraw(RoadConstructionParser.DrawContext ctx) {

    }

    @Override
    public void enterAutoMarkup(RoadConstructionParser.AutoMarkupContext ctx) {

    }

    @Override
    public void exitAutoMarkup(RoadConstructionParser.AutoMarkupContext ctx) {

    }

    @Override
    public void enterSetter(RoadConstructionParser.SetterContext ctx) {

    }

    @Override
    public void exitSetter(RoadConstructionParser.SetterContext ctx) {

    }

    @Override
    public void enterDimensional(RoadConstructionParser.DimensionalContext ctx) {

    }

    @Override
    public void exitDimensional(RoadConstructionParser.DimensionalContext ctx) {

    }

    @Override
    public void enterSetWidth(RoadConstructionParser.SetWidthContext ctx) {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!! " + ctx.getStart().toString() + " !!!!!!!!!!!!!!!!!!!!!!");

        String input = tokenInputParser.getUserInputFromToken(ctx.getStart().toString());
        String[] args = EventParser.getInputArguments(input);

        System.out.println("Width BEFORE setWidth: " + roadHashMap.get(args[0]).getWidth());
        roadHashMap.get(args[0]).setWidth(Integer.parseInt(args[1]));
        System.out.println("Width AFTER setWidth: " + roadHashMap.get(args[0]).getWidth());

        //SetWidth setWidth = new SetWidth(tokenInputParser.getUserInputFromToken(ctx.getStart().toString()), roadHashMap);
        //System.out.println("Width in setWidth: " + roadHashMap.get(setWidth.getArg1()).getWidth());
    }

    @Override
    public void exitSetWidth(RoadConstructionParser.SetWidthContext ctx) {

    }

    @Override
    public void enterSetLength(RoadConstructionParser.SetLengthContext ctx) {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!! " + ctx.getStart().toString() + " !!!!!!!!!!!!!!!!!!!!!!");

        String input = tokenInputParser.getUserInputFromToken(ctx.getStart().toString());
        String[] args = EventParser.getInputArguments(input);

        System.out.println("Length BEFORE setLength: " + roadHashMap.get(args[0]).getLength());
        roadHashMap.get(args[0]).setLength(Integer.parseInt(args[1]));
        System.out.println("Length AFTER setLength: " + roadHashMap.get(args[0]).getLength());

        // SetLength setLength = new SetLength(tokenInputParser.getUserInputFromToken(ctx.getStart().toString()), roadHashMap);
        // System.out.println("Length in setLength: " + roadHashMap.get(setLength.getArg1()).getLength());
    }

    @Override
    public void exitSetLength(RoadConstructionParser.SetLengthContext ctx) {

    }

    @Override
    public void enterSetCrossingAngle(RoadConstructionParser.SetCrossingAngleContext ctx) {
        System.out.println("enterSetCrossingAngle content: " + ctx.getStart().toString());

        String input = tokenInputParser.getUserInputFromToken(ctx.getStart().toString());
        String[] args = EventParser.getInputArguments(input);

        System.out.println("Angle BEFORE SetCrossingAngle: " + roadHashMap.get(args[0]).getAngle());
        roadHashMap.get(args[0]).setAngle(Integer.parseInt(args[1]));
        System.out.println("Angle AFTER SetCrossingAngle: " + roadHashMap.get(args[0]).getAngle());
    }

    @Override
    public void exitSetCrossingAngle(RoadConstructionParser.SetCrossingAngleContext ctx) {

    }

    @Override
    public void enterRestrictional(RoadConstructionParser.RestrictionalContext ctx) {

    }

    @Override
    public void exitRestrictional(RoadConstructionParser.RestrictionalContext ctx) {

    }

    @Override
    public void enterSetLongtitudinalMarking(RoadConstructionParser.SetLongtitudinalMarkingContext ctx) {

    }

    @Override
    public void exitSetLongtitudinalMarking(RoadConstructionParser.SetLongtitudinalMarkingContext ctx) {

    }

    @Override
    public void enterSetPriority(RoadConstructionParser.SetPriorityContext ctx) {

    }

    @Override
    public void exitSetPriority(RoadConstructionParser.SetPriorityContext ctx) {

    }

    @Override
    public void enterSetIntersectionType(RoadConstructionParser.SetIntersectionTypeContext ctx) {

    }

    @Override
    public void exitSetIntersectionType(RoadConstructionParser.SetIntersectionTypeContext ctx) {

    }

    @Override
    public void enterSetMovementType(RoadConstructionParser.SetMovementTypeContext ctx) {

    }

    @Override
    public void exitSetMovementType(RoadConstructionParser.SetMovementTypeContext ctx) {

    }

    @Override
    public void enterCreateRoad(RoadConstructionParser.CreateRoadContext ctx) {
        System.out.println(ANSI_PURPLE + "enterCreateRoad invoked!" + ANSI_RESET);
        System.out.println(ctx.getStart().toString());

        Road road = new Road(tokenInputParser.getUserInputFromToken(ctx.getStart().toString()));
        appArrayList.add(road);
        System.out.println("road.getvarname() = " + road.getVarName());
        roadHashMap.put(road.getVarName(), road);
        appHashMap.put(road.getVarName(), road);
        System.out.println("Road name: " + road.getVarName());
        System.out.println("Width in createRoad: " + roadHashMap.get(road.getVarName()).getWidth());
    }

    @Override
    public void exitCreateRoad(RoadConstructionParser.CreateRoadContext ctx) {

    }

    @Override
    public void enterCreateIntersection(RoadConstructionParser.CreateIntersectionContext ctx) {
        System.out.println(ANSI_PURPLE + "enterCreateIntersection invoked!" + ANSI_RESET);
        System.out.println(ctx.getStart().toString());

        Intersection intersection = new Intersection(tokenInputParser.getUserInputFromToken(ctx.getStart().toString()));
        appArrayList.add(intersection);
        System.out.println("sign.getvarname() = " + intersection.getVarName());
        appHashMap.put(intersection.getVarName(), intersection);
        System.out.println("Sign name: " + intersection.getVarName());
        //System.out.println("Type in createSign: " + appHashMap.get(sign.getVarName()));
    }

    @Override
    public void exitCreateIntersection(RoadConstructionParser.CreateIntersectionContext ctx) {

    }

    @Override
    public void enterCreateSign(RoadConstructionParser.CreateSignContext ctx) {
        System.out.println("----------------");
        System.out.println(ANSI_PURPLE + "enterCreateSign invoked!" + ANSI_RESET);
        System.out.println(ctx.getStart().toString());

        String input = tokenInputParser.getUserInputFromToken(ctx.getStart().toString());
        String[] args = EventParser.getInputArguments(input);
        System.err.println("after getInputArguments in createSign");
        for (int i = 0; i < args.length; i++) {

            System.out.println(ANSI_CYAN + args[i] + ANSI_RESET);
        }

        Sign sign = new Sign(tokenInputParser.getUserInputFromToken(ctx.getStart().toString()), roadHashMap, signHashMap);
        System.out.println(ANSI_RED + tokenInputParser.getUserInputFromToken(ctx.getStart().toString()) + ANSI_RESET);
        appArrayList.add(sign);
        /*System.out.println("sign.getvarname() = " + sign.getVarName());
        signHashMap.put(sign.getVarName(), sign);
        System.out.println(ANSI_RED + signHashMap.get("a") + ANSI_RESET);
        System.out.println("Sign name: " + sign.getVarName());*/

        System.out.println(ANSI_CYAN + signHashMap.get(sign.getVarName()) + ANSI_RESET);

        //System.out.println("Type in createSign: " + appHashMap.get(sign.getVarName()));
    }

    @Override
    public void exitCreateSign(RoadConstructionParser.CreateSignContext ctx) {

    }

    @Override
    public void visitTerminal(TerminalNode terminalNode) {

    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {

    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {

    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {

    }





}
