/*
    This class takes care of testing different kinds
    of input on correctness in compliance with the parser
    generated with the help of grammar by ANTLR4.
*/

package src.base.utils;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import src.ANTLR.RoadConstructionLexer;
import src.ANTLR.RoadConstructionParser;
import src.base.common.App;

import static src.base.common.Constants.*;

public class RoadConstructionTestParser {
    private final String input;
    private RoadConstructionParser parser;

    public RoadConstructionTestParser(final String input, App app) {
        this.input = input;
        final RoadConstructionLexer lexer = new RoadConstructionLexer(CharStreams.fromString(this.input));
        parser = new RoadConstructionParser(new CommonTokenStream(lexer));
        parser.addParseListener(new RoadConstructionListener(app));
        parser.addErrorListener(new ErrorRoadConstructionListener());
    }

    private String printResultHeader(){
        return ANSI_GREEN + "Parse result:" + ANSI_RESET + "\n";
    }

    // ------------------------------- Input parsers -------------------------------

    public String parse() {
        // reutrn the whole text till the end
        return  printResultHeader() + parser.program().getText() + "\n\n";
    }

    public String parseDeclareRoadVar(){
        // return first ROAD element declaration
        return printResultHeader() + parser.program().statement(0).createElement().createRoad().ROAD().getText() + "\n\n";
    }

    public String parseDeclareSignVar(){
        // return first Sign element declaration
        return printResultHeader() + parser.program().statement(0).createElement().createSign().SIGN().getText() + "\n\n";
    }

    public String parseSetWidth(){
        // return first ROAD element declaration
        return printResultHeader() + parser.program().statement(0).invokeProcedure().setter().dimensional().setWidth().SET_WIDTH().getText() + "\n\n";
    }

    public String parseSetLength(){
        // return first ROAD element declaration
        return printResultHeader() + parser.program().statement(0).invokeProcedure().setter().dimensional().setLength().SET_LENGTH().getText() + "\n\n";
    }

}
