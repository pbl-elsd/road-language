package src.deprecated;

import src.base.RoadConstruction.Road;
import src.base.common.EventParser;

import java.util.HashMap;
import java.util.Set;

public class SetLength {
    private HashMap<String, Road> roadHashMap;

    private String[] args;

    public SetLength(String input, HashMap<String, Road> roadHashMap){
        this.roadHashMap = roadHashMap;

        args = EventParser.getInputArguments(input);

        System.out.println("arg1: " + args[0] + " arg2: " + args[1]);

        Set<String> keyset = roadHashMap.keySet();
        System.out.println("roadHashMapKeysetInSetLength: ");
        for(int i = 0; i < keyset.size(); i++)
            System.out.println(keyset.toString());

        System.out.println("Length before SetLength: " + roadHashMap.get(args[0]).getLength());
        setLength();

    }

    public void setLength(){
        Road r = new Road();
        roadHashMap.get(args[0]).setLength(Integer.parseInt(args[1]));
        System.out.println("length right after setLength finishes: " + roadHashMap.get(args[0]).getLength());
    }

    public String getArg1(){
        return args[0];
    }

    public String getArg2(){
        return args[1];
    }
}