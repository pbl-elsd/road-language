package src.deprecated;

import src.base.RoadConstruction.Road;
import src.base.common.EventParser;

import java.util.HashMap;
import java.util.Set;

public class SetWidth {
    private HashMap<String, Road> roadHashMap;

    private String input;
    private String[] args;

    public SetWidth(String input, HashMap<String, Road> roadHashMap){
        this.input = input.trim();
        this.roadHashMap = roadHashMap;

        args = EventParser.getInputArguments(input);

        System.out.println("arg1: " + args[0] + " arg2: " + args[1]);

        Set<String> keyset = roadHashMap.keySet();
        System.out.println("roadHashMapKeysetInSetWidth: ");
        for(int i = 0; i < keyset.size(); i++)
            System.out.println(keyset.toString());

        System.out.println("Width before setWidth: " + roadHashMap.get(args[0]).getWidth());
        setWidth();

    }

    public void setWidth(){
        Road r = new Road();
        roadHashMap.get(args[0]).setWidth(Integer.parseInt(args[1]));
        System.out.println("width right after setWidth finishes: " + roadHashMap.get(args[0]).getWidth());
    }

    public String getArg1(){
        return args[0];
    }

    public String getArg2(){
        return args[1];
    }
}
