module org.example {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.antlr.antlr4.runtime;

    opens org.Main to javafx.fxml;
    exports org.Main;
    exports org.VX;
    opens org.VX to javafx.fxml;
}