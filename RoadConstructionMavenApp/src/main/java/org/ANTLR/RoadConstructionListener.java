// Generated from D:/!Works/UTM/Domain-Specific-Language-Project/Project/road-language/ANTLR/src/Grammars\RoadConstruction.g4 by ANTLR 4.9.1
package org.ANTLR;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RoadConstructionParser}.
 */
public interface RoadConstructionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(RoadConstructionParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(RoadConstructionParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(RoadConstructionParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(RoadConstructionParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#createElement}.
	 * @param ctx the parse tree
	 */
	void enterCreateElement(RoadConstructionParser.CreateElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#createElement}.
	 * @param ctx the parse tree
	 */
	void exitCreateElement(RoadConstructionParser.CreateElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#invokeProcedure}.
	 * @param ctx the parse tree
	 */
	void enterInvokeProcedure(RoadConstructionParser.InvokeProcedureContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#invokeProcedure}.
	 * @param ctx the parse tree
	 */
	void exitInvokeProcedure(RoadConstructionParser.InvokeProcedureContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#finalize}.
	 * @param ctx the parse tree
	 */
	void enterFinalize(RoadConstructionParser.FinalizeContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#finalize}.
	 * @param ctx the parse tree
	 */
	void exitFinalize(RoadConstructionParser.FinalizeContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#draw}.
	 * @param ctx the parse tree
	 */
	void enterDraw(RoadConstructionParser.DrawContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#draw}.
	 * @param ctx the parse tree
	 */
	void exitDraw(RoadConstructionParser.DrawContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#autoMarkup}.
	 * @param ctx the parse tree
	 */
	void enterAutoMarkup(RoadConstructionParser.AutoMarkupContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#autoMarkup}.
	 * @param ctx the parse tree
	 */
	void exitAutoMarkup(RoadConstructionParser.AutoMarkupContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setter}.
	 * @param ctx the parse tree
	 */
	void enterSetter(RoadConstructionParser.SetterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setter}.
	 * @param ctx the parse tree
	 */
	void exitSetter(RoadConstructionParser.SetterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#dimensional}.
	 * @param ctx the parse tree
	 */
	void enterDimensional(RoadConstructionParser.DimensionalContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#dimensional}.
	 * @param ctx the parse tree
	 */
	void exitDimensional(RoadConstructionParser.DimensionalContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setWidth}.
	 * @param ctx the parse tree
	 */
	void enterSetWidth(RoadConstructionParser.SetWidthContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setWidth}.
	 * @param ctx the parse tree
	 */
	void exitSetWidth(RoadConstructionParser.SetWidthContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setLength}.
	 * @param ctx the parse tree
	 */
	void enterSetLength(RoadConstructionParser.SetLengthContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setLength}.
	 * @param ctx the parse tree
	 */
	void exitSetLength(RoadConstructionParser.SetLengthContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setCrossingAngle}.
	 * @param ctx the parse tree
	 */
	void enterSetCrossingAngle(RoadConstructionParser.SetCrossingAngleContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setCrossingAngle}.
	 * @param ctx the parse tree
	 */
	void exitSetCrossingAngle(RoadConstructionParser.SetCrossingAngleContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#restrictional}.
	 * @param ctx the parse tree
	 */
	void enterRestrictional(RoadConstructionParser.RestrictionalContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#restrictional}.
	 * @param ctx the parse tree
	 */
	void exitRestrictional(RoadConstructionParser.RestrictionalContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setLongtitudinalMarking}.
	 * @param ctx the parse tree
	 */
	void enterSetLongtitudinalMarking(RoadConstructionParser.SetLongtitudinalMarkingContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setLongtitudinalMarking}.
	 * @param ctx the parse tree
	 */
	void exitSetLongtitudinalMarking(RoadConstructionParser.SetLongtitudinalMarkingContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setPriority}.
	 * @param ctx the parse tree
	 */
	void enterSetPriority(RoadConstructionParser.SetPriorityContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setPriority}.
	 * @param ctx the parse tree
	 */
	void exitSetPriority(RoadConstructionParser.SetPriorityContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setIntersectionType}.
	 * @param ctx the parse tree
	 */
	void enterSetIntersectionType(RoadConstructionParser.SetIntersectionTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setIntersectionType}.
	 * @param ctx the parse tree
	 */
	void exitSetIntersectionType(RoadConstructionParser.SetIntersectionTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#setMovementType}.
	 * @param ctx the parse tree
	 */
	void enterSetMovementType(RoadConstructionParser.SetMovementTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#setMovementType}.
	 * @param ctx the parse tree
	 */
	void exitSetMovementType(RoadConstructionParser.SetMovementTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#createRoad}.
	 * @param ctx the parse tree
	 */
	void enterCreateRoad(RoadConstructionParser.CreateRoadContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#createRoad}.
	 * @param ctx the parse tree
	 */
	void exitCreateRoad(RoadConstructionParser.CreateRoadContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#createIntersection}.
	 * @param ctx the parse tree
	 */
	void enterCreateIntersection(RoadConstructionParser.CreateIntersectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#createIntersection}.
	 * @param ctx the parse tree
	 */
	void exitCreateIntersection(RoadConstructionParser.CreateIntersectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RoadConstructionParser#createSign}.
	 * @param ctx the parse tree
	 */
	void enterCreateSign(RoadConstructionParser.CreateSignContext ctx);
	/**
	 * Exit a parse tree produced by {@link RoadConstructionParser#createSign}.
	 * @param ctx the parse tree
	 */
	void exitCreateSign(RoadConstructionParser.CreateSignContext ctx);
}