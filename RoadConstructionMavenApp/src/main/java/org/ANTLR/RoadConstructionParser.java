// Generated from D:/!Works/UTM/Domain-Specific-Language-Project/Project/road-language/ANTLR/src/Grammars\RoadConstruction.g4 by ANTLR 4.9.1
package org.ANTLR;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RoadConstructionParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NEWLINE=1, STMTEND=2, ROAD=3, INTERSECTION=4, SIGN=5, SIGN_TYPE=6, OFFSET=7, 
		SIDE=8, INT=9, SET_WIDTH=10, SET_LENGTH=11, SET_CROSSING_ANGLE=12, SET_INTERSECTION_WIDTH=13, 
		SET_INTERSECTION_LENGTH=14, ADD_INTERSECTING_ROAD=15, SET_LONGTITUDINAL_MAKING=16, 
		SET_PRIORITY=17, SET_INTERSECTION_TYPE=18, SET_MOVEMENT_TYPE=19, DRAW=20, 
		AUTO_MARKUP=21;
	public static final int
		RULE_program = 0, RULE_statement = 1, RULE_createElement = 2, RULE_invokeProcedure = 3, 
		RULE_finalize = 4, RULE_draw = 5, RULE_autoMarkup = 6, RULE_setter = 7, 
		RULE_dimensional = 8, RULE_setWidth = 9, RULE_setLength = 10, RULE_setCrossingAngle = 11, 
		RULE_restrictional = 12, RULE_setLongtitudinalMarking = 13, RULE_setPriority = 14, 
		RULE_setIntersectionType = 15, RULE_setMovementType = 16, RULE_createRoad = 17, 
		RULE_createIntersection = 18, RULE_createSign = 19;
	private static String[] makeRuleNames() {
		return new String[] {
			"program", "statement", "createElement", "invokeProcedure", "finalize", 
			"draw", "autoMarkup", "setter", "dimensional", "setWidth", "setLength", 
			"setCrossingAngle", "restrictional", "setLongtitudinalMarking", "setPriority", 
			"setIntersectionType", "setMovementType", "createRoad", "createIntersection", 
			"createSign"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, "'Draw()'", "'AutoMarkup()'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "NEWLINE", "STMTEND", "ROAD", "INTERSECTION", "SIGN", "SIGN_TYPE", 
			"OFFSET", "SIDE", "INT", "SET_WIDTH", "SET_LENGTH", "SET_CROSSING_ANGLE", 
			"SET_INTERSECTION_WIDTH", "SET_INTERSECTION_LENGTH", "ADD_INTERSECTING_ROAD", 
			"SET_LONGTITUDINAL_MAKING", "SET_PRIORITY", "SET_INTERSECTION_TYPE", 
			"SET_MOVEMENT_TYPE", "DRAW", "AUTO_MARKUP"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "RoadConstruction.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public RoadConstructionParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgramContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<TerminalNode> STMTEND() { return getTokens(RoadConstructionParser.STMTEND); }
		public TerminalNode STMTEND(int i) {
			return getToken(RoadConstructionParser.STMTEND, i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(40);
				statement();
				setState(41);
				match(STMTEND);
				}
				}
				setState(45); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ROAD) | (1L << INTERSECTION) | (1L << SIGN) | (1L << SET_WIDTH) | (1L << SET_LENGTH) | (1L << SET_CROSSING_ANGLE) | (1L << SET_LONGTITUDINAL_MAKING) | (1L << SET_PRIORITY) | (1L << SET_INTERSECTION_TYPE) | (1L << SET_MOVEMENT_TYPE) | (1L << DRAW) | (1L << AUTO_MARKUP))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public CreateElementContext createElement() {
			return getRuleContext(CreateElementContext.class,0);
		}
		public InvokeProcedureContext invokeProcedure() {
			return getRuleContext(InvokeProcedureContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			setState(49);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ROAD:
			case INTERSECTION:
			case SIGN:
				enterOuterAlt(_localctx, 1);
				{
				setState(47);
				createElement();
				}
				break;
			case SET_WIDTH:
			case SET_LENGTH:
			case SET_CROSSING_ANGLE:
			case SET_LONGTITUDINAL_MAKING:
			case SET_PRIORITY:
			case SET_INTERSECTION_TYPE:
			case SET_MOVEMENT_TYPE:
			case DRAW:
			case AUTO_MARKUP:
				enterOuterAlt(_localctx, 2);
				{
				setState(48);
				invokeProcedure();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateElementContext extends ParserRuleContext {
		public CreateRoadContext createRoad() {
			return getRuleContext(CreateRoadContext.class,0);
		}
		public CreateIntersectionContext createIntersection() {
			return getRuleContext(CreateIntersectionContext.class,0);
		}
		public CreateSignContext createSign() {
			return getRuleContext(CreateSignContext.class,0);
		}
		public CreateElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterCreateElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitCreateElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitCreateElement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CreateElementContext createElement() throws RecognitionException {
		CreateElementContext _localctx = new CreateElementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_createElement);
		try {
			setState(54);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ROAD:
				enterOuterAlt(_localctx, 1);
				{
				setState(51);
				createRoad();
				}
				break;
			case INTERSECTION:
				enterOuterAlt(_localctx, 2);
				{
				setState(52);
				createIntersection();
				}
				break;
			case SIGN:
				enterOuterAlt(_localctx, 3);
				{
				setState(53);
				createSign();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InvokeProcedureContext extends ParserRuleContext {
		public FinalizeContext finalizeNew() {
			return getRuleContext(FinalizeContext.class,0);
		}
		public SetterContext setter() {
			return getRuleContext(SetterContext.class,0);
		}
		public InvokeProcedureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_invokeProcedure; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterInvokeProcedure(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitInvokeProcedure(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitInvokeProcedure(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InvokeProcedureContext invokeProcedure() throws RecognitionException {
		InvokeProcedureContext _localctx = new InvokeProcedureContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_invokeProcedure);
		try {
			setState(58);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DRAW:
			case AUTO_MARKUP:
				enterOuterAlt(_localctx, 1);
				{
				setState(56);
				finalizeNew();
				}
				break;
			case SET_WIDTH:
			case SET_LENGTH:
			case SET_CROSSING_ANGLE:
			case SET_LONGTITUDINAL_MAKING:
			case SET_PRIORITY:
			case SET_INTERSECTION_TYPE:
			case SET_MOVEMENT_TYPE:
				enterOuterAlt(_localctx, 2);
				{
				setState(57);
				setter();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FinalizeContext extends ParserRuleContext {
		public DrawContext draw() {
			return getRuleContext(DrawContext.class,0);
		}
		public AutoMarkupContext autoMarkup() {
			return getRuleContext(AutoMarkupContext.class,0);
		}
		public FinalizeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_finalize; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterFinalize(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitFinalize(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitFinalize(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FinalizeContext finalizeNew() throws RecognitionException {
		FinalizeContext _localctx = new FinalizeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_finalize);
		try {
			setState(62);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DRAW:
				enterOuterAlt(_localctx, 1);
				{
				setState(60);
				draw();
				}
				break;
			case AUTO_MARKUP:
				enterOuterAlt(_localctx, 2);
				{
				setState(61);
				autoMarkup();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DrawContext extends ParserRuleContext {
		public TerminalNode DRAW() { return getToken(RoadConstructionParser.DRAW, 0); }
		public DrawContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_draw; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterDraw(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitDraw(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitDraw(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DrawContext draw() throws RecognitionException {
		DrawContext _localctx = new DrawContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_draw);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64);
			match(DRAW);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AutoMarkupContext extends ParserRuleContext {
		public TerminalNode AUTO_MARKUP() { return getToken(RoadConstructionParser.AUTO_MARKUP, 0); }
		public AutoMarkupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_autoMarkup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterAutoMarkup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitAutoMarkup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitAutoMarkup(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AutoMarkupContext autoMarkup() throws RecognitionException {
		AutoMarkupContext _localctx = new AutoMarkupContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_autoMarkup);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(66);
			match(AUTO_MARKUP);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetterContext extends ParserRuleContext {
		public DimensionalContext dimensional() {
			return getRuleContext(DimensionalContext.class,0);
		}
		public RestrictionalContext restrictional() {
			return getRuleContext(RestrictionalContext.class,0);
		}
		public SetterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetterContext setter() throws RecognitionException {
		SetterContext _localctx = new SetterContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_setter);
		try {
			setState(70);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SET_WIDTH:
			case SET_LENGTH:
			case SET_CROSSING_ANGLE:
				enterOuterAlt(_localctx, 1);
				{
				setState(68);
				dimensional();
				}
				break;
			case SET_LONGTITUDINAL_MAKING:
			case SET_PRIORITY:
			case SET_INTERSECTION_TYPE:
			case SET_MOVEMENT_TYPE:
				enterOuterAlt(_localctx, 2);
				{
				setState(69);
				restrictional();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DimensionalContext extends ParserRuleContext {
		public SetWidthContext setWidth() {
			return getRuleContext(SetWidthContext.class,0);
		}
		public SetLengthContext setLength() {
			return getRuleContext(SetLengthContext.class,0);
		}
		public SetCrossingAngleContext setCrossingAngle() {
			return getRuleContext(SetCrossingAngleContext.class,0);
		}
		public DimensionalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dimensional; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterDimensional(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitDimensional(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitDimensional(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DimensionalContext dimensional() throws RecognitionException {
		DimensionalContext _localctx = new DimensionalContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_dimensional);
		try {
			setState(75);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SET_WIDTH:
				enterOuterAlt(_localctx, 1);
				{
				setState(72);
				setWidth();
				}
				break;
			case SET_LENGTH:
				enterOuterAlt(_localctx, 2);
				{
				setState(73);
				setLength();
				}
				break;
			case SET_CROSSING_ANGLE:
				enterOuterAlt(_localctx, 3);
				{
				setState(74);
				setCrossingAngle();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetWidthContext extends ParserRuleContext {
		public TerminalNode SET_WIDTH() { return getToken(RoadConstructionParser.SET_WIDTH, 0); }
		public SetWidthContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setWidth; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetWidth(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetWidth(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetWidth(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetWidthContext setWidth() throws RecognitionException {
		SetWidthContext _localctx = new SetWidthContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_setWidth);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			match(SET_WIDTH);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetLengthContext extends ParserRuleContext {
		public TerminalNode SET_LENGTH() { return getToken(RoadConstructionParser.SET_LENGTH, 0); }
		public SetLengthContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setLength; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetLength(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetLength(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetLength(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetLengthContext setLength() throws RecognitionException {
		SetLengthContext _localctx = new SetLengthContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_setLength);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79);
			match(SET_LENGTH);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetCrossingAngleContext extends ParserRuleContext {
		public TerminalNode SET_CROSSING_ANGLE() { return getToken(RoadConstructionParser.SET_CROSSING_ANGLE, 0); }
		public SetCrossingAngleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setCrossingAngle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetCrossingAngle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetCrossingAngle(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetCrossingAngle(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetCrossingAngleContext setCrossingAngle() throws RecognitionException {
		SetCrossingAngleContext _localctx = new SetCrossingAngleContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_setCrossingAngle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			match(SET_CROSSING_ANGLE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RestrictionalContext extends ParserRuleContext {
		public SetLongtitudinalMarkingContext setLongtitudinalMarking() {
			return getRuleContext(SetLongtitudinalMarkingContext.class,0);
		}
		public SetPriorityContext setPriority() {
			return getRuleContext(SetPriorityContext.class,0);
		}
		public SetIntersectionTypeContext setIntersectionType() {
			return getRuleContext(SetIntersectionTypeContext.class,0);
		}
		public SetMovementTypeContext setMovementType() {
			return getRuleContext(SetMovementTypeContext.class,0);
		}
		public RestrictionalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_restrictional; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterRestrictional(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitRestrictional(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitRestrictional(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RestrictionalContext restrictional() throws RecognitionException {
		RestrictionalContext _localctx = new RestrictionalContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_restrictional);
		try {
			setState(87);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SET_LONGTITUDINAL_MAKING:
				enterOuterAlt(_localctx, 1);
				{
				setState(83);
				setLongtitudinalMarking();
				}
				break;
			case SET_PRIORITY:
				enterOuterAlt(_localctx, 2);
				{
				setState(84);
				setPriority();
				}
				break;
			case SET_INTERSECTION_TYPE:
				enterOuterAlt(_localctx, 3);
				{
				setState(85);
				setIntersectionType();
				}
				break;
			case SET_MOVEMENT_TYPE:
				enterOuterAlt(_localctx, 4);
				{
				setState(86);
				setMovementType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetLongtitudinalMarkingContext extends ParserRuleContext {
		public TerminalNode SET_LONGTITUDINAL_MAKING() { return getToken(RoadConstructionParser.SET_LONGTITUDINAL_MAKING, 0); }
		public SetLongtitudinalMarkingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setLongtitudinalMarking; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetLongtitudinalMarking(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetLongtitudinalMarking(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetLongtitudinalMarking(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetLongtitudinalMarkingContext setLongtitudinalMarking() throws RecognitionException {
		SetLongtitudinalMarkingContext _localctx = new SetLongtitudinalMarkingContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_setLongtitudinalMarking);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			match(SET_LONGTITUDINAL_MAKING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetPriorityContext extends ParserRuleContext {
		public TerminalNode SET_PRIORITY() { return getToken(RoadConstructionParser.SET_PRIORITY, 0); }
		public SetPriorityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setPriority; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetPriority(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetPriority(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetPriority(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetPriorityContext setPriority() throws RecognitionException {
		SetPriorityContext _localctx = new SetPriorityContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_setPriority);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(91);
			match(SET_PRIORITY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetIntersectionTypeContext extends ParserRuleContext {
		public TerminalNode SET_INTERSECTION_TYPE() { return getToken(RoadConstructionParser.SET_INTERSECTION_TYPE, 0); }
		public SetIntersectionTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setIntersectionType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetIntersectionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetIntersectionType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetIntersectionType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetIntersectionTypeContext setIntersectionType() throws RecognitionException {
		SetIntersectionTypeContext _localctx = new SetIntersectionTypeContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_setIntersectionType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93);
			match(SET_INTERSECTION_TYPE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetMovementTypeContext extends ParserRuleContext {
		public TerminalNode SET_MOVEMENT_TYPE() { return getToken(RoadConstructionParser.SET_MOVEMENT_TYPE, 0); }
		public SetMovementTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setMovementType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterSetMovementType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitSetMovementType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitSetMovementType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SetMovementTypeContext setMovementType() throws RecognitionException {
		SetMovementTypeContext _localctx = new SetMovementTypeContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_setMovementType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			match(SET_MOVEMENT_TYPE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateRoadContext extends ParserRuleContext {
		public TerminalNode ROAD() { return getToken(RoadConstructionParser.ROAD, 0); }
		public CreateRoadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createRoad; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterCreateRoad(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitCreateRoad(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitCreateRoad(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CreateRoadContext createRoad() throws RecognitionException {
		CreateRoadContext _localctx = new CreateRoadContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_createRoad);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			match(ROAD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateIntersectionContext extends ParserRuleContext {
		public TerminalNode INTERSECTION() { return getToken(RoadConstructionParser.INTERSECTION, 0); }
		public CreateIntersectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createIntersection; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterCreateIntersection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitCreateIntersection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitCreateIntersection(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CreateIntersectionContext createIntersection() throws RecognitionException {
		CreateIntersectionContext _localctx = new CreateIntersectionContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_createIntersection);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			match(INTERSECTION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateSignContext extends ParserRuleContext {
		public TerminalNode SIGN() { return getToken(RoadConstructionParser.SIGN, 0); }
		public CreateSignContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createSign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).enterCreateSign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RoadConstructionListener) ((RoadConstructionListener)listener).exitCreateSign(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RoadConstructionVisitor) return ((RoadConstructionVisitor<? extends T>)visitor).visitCreateSign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CreateSignContext createSign() throws RecognitionException {
		CreateSignContext _localctx = new CreateSignContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_createSign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			match(SIGN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\27j\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23"+
		"\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\6\2.\n\2\r\2\16\2/\3\3\3\3\5\3\64"+
		"\n\3\3\4\3\4\3\4\5\49\n\4\3\5\3\5\5\5=\n\5\3\6\3\6\5\6A\n\6\3\7\3\7\3"+
		"\b\3\b\3\t\3\t\5\tI\n\t\3\n\3\n\3\n\5\nN\n\n\3\13\3\13\3\f\3\f\3\r\3\r"+
		"\3\16\3\16\3\16\3\16\5\16Z\n\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22"+
		"\3\23\3\23\3\24\3\24\3\25\3\25\3\25\2\2\26\2\4\6\b\n\f\16\20\22\24\26"+
		"\30\32\34\36 \"$&(\2\2\2a\2-\3\2\2\2\4\63\3\2\2\2\68\3\2\2\2\b<\3\2\2"+
		"\2\n@\3\2\2\2\fB\3\2\2\2\16D\3\2\2\2\20H\3\2\2\2\22M\3\2\2\2\24O\3\2\2"+
		"\2\26Q\3\2\2\2\30S\3\2\2\2\32Y\3\2\2\2\34[\3\2\2\2\36]\3\2\2\2 _\3\2\2"+
		"\2\"a\3\2\2\2$c\3\2\2\2&e\3\2\2\2(g\3\2\2\2*+\5\4\3\2+,\7\4\2\2,.\3\2"+
		"\2\2-*\3\2\2\2./\3\2\2\2/-\3\2\2\2/\60\3\2\2\2\60\3\3\2\2\2\61\64\5\6"+
		"\4\2\62\64\5\b\5\2\63\61\3\2\2\2\63\62\3\2\2\2\64\5\3\2\2\2\659\5$\23"+
		"\2\669\5&\24\2\679\5(\25\28\65\3\2\2\28\66\3\2\2\28\67\3\2\2\29\7\3\2"+
		"\2\2:=\5\n\6\2;=\5\20\t\2<:\3\2\2\2<;\3\2\2\2=\t\3\2\2\2>A\5\f\7\2?A\5"+
		"\16\b\2@>\3\2\2\2@?\3\2\2\2A\13\3\2\2\2BC\7\26\2\2C\r\3\2\2\2DE\7\27\2"+
		"\2E\17\3\2\2\2FI\5\22\n\2GI\5\32\16\2HF\3\2\2\2HG\3\2\2\2I\21\3\2\2\2"+
		"JN\5\24\13\2KN\5\26\f\2LN\5\30\r\2MJ\3\2\2\2MK\3\2\2\2ML\3\2\2\2N\23\3"+
		"\2\2\2OP\7\f\2\2P\25\3\2\2\2QR\7\r\2\2R\27\3\2\2\2ST\7\16\2\2T\31\3\2"+
		"\2\2UZ\5\34\17\2VZ\5\36\20\2WZ\5 \21\2XZ\5\"\22\2YU\3\2\2\2YV\3\2\2\2"+
		"YW\3\2\2\2YX\3\2\2\2Z\33\3\2\2\2[\\\7\22\2\2\\\35\3\2\2\2]^\7\23\2\2^"+
		"\37\3\2\2\2_`\7\24\2\2`!\3\2\2\2ab\7\25\2\2b#\3\2\2\2cd\7\5\2\2d%\3\2"+
		"\2\2ef\7\6\2\2f\'\3\2\2\2gh\7\7\2\2h)\3\2\2\2\n/\638<@HMY";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}