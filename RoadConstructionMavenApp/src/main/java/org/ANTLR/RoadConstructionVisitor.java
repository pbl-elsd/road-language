// Generated from D:/!Works/UTM/Domain-Specific-Language-Project/Project/road-language/ANTLR/src/Grammars\RoadConstruction.g4 by ANTLR 4.9.1
package org.ANTLR;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link RoadConstructionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface RoadConstructionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(RoadConstructionParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(RoadConstructionParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#createElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateElement(RoadConstructionParser.CreateElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#invokeProcedure}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInvokeProcedure(RoadConstructionParser.InvokeProcedureContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#finalize}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFinalize(RoadConstructionParser.FinalizeContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#draw}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDraw(RoadConstructionParser.DrawContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#autoMarkup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAutoMarkup(RoadConstructionParser.AutoMarkupContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetter(RoadConstructionParser.SetterContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#dimensional}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDimensional(RoadConstructionParser.DimensionalContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setWidth}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetWidth(RoadConstructionParser.SetWidthContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setLength}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetLength(RoadConstructionParser.SetLengthContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setCrossingAngle}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetCrossingAngle(RoadConstructionParser.SetCrossingAngleContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#restrictional}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRestrictional(RoadConstructionParser.RestrictionalContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setLongtitudinalMarking}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetLongtitudinalMarking(RoadConstructionParser.SetLongtitudinalMarkingContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setPriority}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetPriority(RoadConstructionParser.SetPriorityContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setIntersectionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetIntersectionType(RoadConstructionParser.SetIntersectionTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#setMovementType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetMovementType(RoadConstructionParser.SetMovementTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#createRoad}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateRoad(RoadConstructionParser.CreateRoadContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#createIntersection}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateIntersection(RoadConstructionParser.CreateIntersectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RoadConstructionParser#createSign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateSign(RoadConstructionParser.CreateSignContext ctx);
}