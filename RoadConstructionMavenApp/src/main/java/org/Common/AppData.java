package org.Common;

import org.RoadConstruction.Intersection;
import org.RoadConstruction.Road;
import org.RoadConstruction.Sign;

import java.util.ArrayList;
import java.util.HashMap;

public class AppData {
    public HashMap<String, Object> appHashMap = null;
    public ArrayList<Object> appArrayList = null;
    public HashMap<String, Road> roadHashMap = null;
    public HashMap<String, Intersection> intersectionHashMap = null;
    public HashMap<String, Sign> signHashMap = null;

    public AppData() {
        appHashMap = new HashMap<>();
        appArrayList = new ArrayList<>();
        roadHashMap = new HashMap<>();
        intersectionHashMap = new HashMap<>();
        signHashMap = new HashMap<>();
    }

}
