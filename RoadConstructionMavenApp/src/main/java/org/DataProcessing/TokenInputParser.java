package org.DataProcessing;

public class TokenInputParser {
    private String input;
    private int from, to;

    public TokenInputParser(){
    }

    public String getUserInputFromToken(String input){
        for(int i = 0; i < input.length(); i++){
            if(input.charAt(i) == '\'') {
                from = i + 1;
                break;
            }
        }

        for(int i = from + 1; i < input.length(); i++){
            if(input.charAt(i) == '\'') {
                to = i;
                break;
            }
        }

        return input.substring(from, to);
    }
}
