// Grammar of the RoadConstruction DSL

/* A Match lexer rule or fragment named A
        A B             Match A followed by B
        (A|B)           Match either A or B
        'text'          Match literal "text"
        A?              Match A zero or one time
        A*              Match A zero or more times
        A+              Match A one or more times
        [A-Z0-9]        Match one character in the defined ranges (in this example between A-Z or 0-9)
        'a'..'z'        Alternative syntax for a character range
        ~[A-Z]          Negation of a range - match any single character not in the range
        .               Match any single character
*/

grammar RoadConstruction;

// ---------------- Parser rules ----------------

program: (statement STMTEND)+;

statement: createElement | invokeProcedure;

// types of statements
createElement: createRoad | createIntersection | createSign;
invokeProcedure: finalize | setter ;

            // !procedures

        // !!finalize
        finalize: draw | autoMarkup;
// draw existing layout
draw: DRAW;
// set the road, intersection properties where they are incorrectly set or unset
autoMarkup: AUTO_MARKUP;

        // !!setters
        setter: dimensional | restrictional;
    // !!!dimensional properties
    dimensional: setWidth | setLength | setCrossingAngle;
// width of the carriage way
setWidth: SET_WIDTH;
// lenght of the road
setLength: SET_LENGTH;
// angle of two merging roads (i.e. two roads are merging under the 30 degree angle)
setCrossingAngle: SET_CROSSING_ANGLE;

    // !!!restricting rules
    restrictional: setLongtitudinalMarking | setPriority | setIntersectionType | setMovementType ;
// type of the longtitudinal road marking (discontinuous, solid)
setLongtitudinalMarking: SET_LONGTITUDINAL_MAKING;
// priority of movement for the road specified (main, give way, stop)
setPriority: SET_PRIORITY;
// type of intersection (regulated, unregulated)
setIntersectionType: SET_INTERSECTION_TYPE;
// type of movement (unilateral, bilateral, reversible)
setMovementType: SET_MOVEMENT_TYPE;


            // !variables

createRoad: ROAD;
createIntersection: INTERSECTION;
createSign: SIGN;



// ---------------- Fragments ----------------

    fragment LOWERCASE  : [a-z];
    fragment UPPERCASE  : [A-Z];
    fragment DIGIT : [0-9];
    fragment LETTER : LOWERCASE | UPPERCASE;
    fragment CHAR : LETTER | DIGIT;
    fragment WORD                : LETTER+ ;
    fragment WHITESPACE          : (' ' | '\t') ;
    fragment TEXT                : ~[\])]+ ;
    fragment VARNAME: (LETTER | '_') (LETTER | DIGIT)*;
    fragment ROAD_NAME : VARNAME;
    fragment INTERSECTION_NAME: VARNAME | (VARNAME '-' VARNAME);
    fragment SIGN_NAME: VARNAME;
    fragment SEMICOLON: ';';

    fragment MARKING_TYPE: 'DoubleSolid' | 'Solid' | 'Broken' | 'Dotted';
    fragment MOVEMENT_TYPE: 'Unilateral' | 'Bilateral' | 'Reversible' ;
    fragment INTERSECTION_TYPE: 'Regulated' | 'Unregulated';
    fragment PRIORITY: 'Main' | 'GiveWay' | 'Stop';



// ---------------- Lexer rules ----------------

    //
    NEWLINE: ('\r\n'|'\n'|'\r');
    STMTEND: SEMICOLON (NEWLINE)* | NEWLINE+;
    ROAD: 'ROAD ' ROAD_NAME;
    INTERSECTION: 'INTERSECTION ' INTERSECTION_NAME;

    SIGN: 'SIGN ' SIGN_NAME '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* SIGN_TYPE ','WHITESPACE* OFFSET ',' WHITESPACE* SIDE ')';
    SIGN_TYPE: 'giveWay' | 'main' | 'crosswalk' | 'roundabout' | 'stop' | 'noEntry' | 'oneWay' | 'noTurnLeft' | 'noTurnAround';
    OFFSET: INT;
    SIDE: 'left' | 'right';

    INT: DIGIT+;

    // SIGN s1(stefan, giveWay, 150, left)

    // !setters
        // !!dimensional setters
    SET_WIDTH:  'SetRoadWidth' '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* INT WHITESPACE*')';
    SET_LENGTH:  'SetRoadLength' '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* INT WHITESPACE*')';
    SET_CROSSING_ANGLE: 'SetCrossingAngle' '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* INT WHITESPACE* ')';
    //SET_CROSSING_ANGLE: 'SetCrossingAngle' '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* INTERSECTION_NAME ',' WHITESPACE* INT WHITESPACE* ')';

    // Intersection setters
    SET_INTERSECTION_WIDTH: 'SetIntersectionWidth' '(' WHITESPACE* INTERSECTION_NAME ',' WHITESPACE* INT WHITESPACE*')';
    SET_INTERSECTION_LENGTH: 'SetIntersectonLength' '(' WHITESPACE* INTERSECTION_NAME ',' WHITESPACE* INT WHITESPACE*')';
    ADD_INTERSECTING_ROAD: 'AddIntersectingRoad' '(' WHITESPACE* INTERSECTION_NAME ',' WHITESPACE* ROAD_NAME WHITESPACE*')' ;

        // !!restrictional setters
    SET_LONGTITUDINAL_MAKING: 'SetLongtitudinalMarking' '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* MARKING_TYPE WHITESPACE* ')';
    SET_PRIORITY: 'SetPriority' '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* INTERSECTION_NAME ',' WHITESPACE* PRIORITY WHITESPACE* ')';
    SET_INTERSECTION_TYPE: 'SetIntersectionType' '(' WHITESPACE* INTERSECTION_NAME ',' WHITESPACE* INTERSECTION_TYPE WHITESPACE* ')';
    SET_MOVEMENT_TYPE: 'SetMovementType' '(' WHITESPACE* ROAD_NAME ',' WHITESPACE* MOVEMENT_TYPE WHITESPACE* ')';

    // !finalize
    DRAW: 'Draw()' ;
    AUTO_MARKUP: 'AutoMarkup()' ;

