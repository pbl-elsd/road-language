package org.Main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.RoadConstruction.Road;
import org.RoadConstruction.Sign;
import org.VX.Elements.RoadVX;
import org.VX.Elements.SignVX;
import org.VX.StageController;
import org.EventsAndParsing.RoadConstructionTestParser;
import org.VX.Vector2;
import org.Common.AppData;

import java.io.IOException;
import java.util.Set;


/**
 * JavaFX AppData
 */
public class App extends Application {

    public static App Instance;

    //private static Scene scene;
    double sceneWidth = 1400;
    double sceneHeight = 800;

    public Vector2 center = new Vector2(sceneWidth/2,  sceneHeight/2);

    StageController _stageController = new StageController();

    AppData appData = new AppData();

    public Group root = new Group();

    TextArea scriptTextField = new TextArea("ROAD a;");
    Button testScriptButton = new Button("Run Script");

    @Override
    public void start(Stage primaryStage) throws IOException {

        Instance = this;



        Scene scene = new Scene(root,sceneWidth,sceneHeight);
        Stage stage = new Stage();

//        TextArea scriptTextField = new TextArea("ROAD a;");
        scriptTextField.setWrapText(true);
        scriptTextField.setLayoutX(50);
        scriptTextField.setLayoutY(30);
        scriptTextField.setPrefWidth(300);
        scriptTextField.setPrefHeight(650);
        scriptTextField.setFont(new Font("Consolas", 20));

//        Button testScriptButton = new Button("Run Script");
        testScriptButton.setLayoutX(130);
        testScriptButton.setLayoutY(700);
        testScriptButton.setFont(new Font("Consolas", 20));

        root.getChildren().add(scriptTextField);
        root.getChildren().add(testScriptButton);

        /*System.out.println(new RoadConstructionTestParser("ROAD Iorga;\n" +
                "ROAD Sciusev;\n" +
                "\n" +
                "SIGN sign(Sciusev, main, 95, left);\n"+
                "\n" +
                "ROAD Iorga;\n" +
                "\n" +
                "SetRoadWidth(Iorga, 5);\n" +
                "SetRoadLength(Iorga, 30);\n" +
                "SetCrossingAngle(Iorga, 65);\n" +
                "SetPriority(Iorga, Iorga-Sciusev, Main);\n" +
                "SetMovementType(Iorga, Bilateral);\n" +
                "SetLongtitudinalMarking(Iorga, DoubleSolid);\n" +
                "\n" +
                "SetRoadWidth(Sciusev, 5);\n" +
                "SetRoadLength(Sciusev, 30);\n" +
                "SetPriority(Sciusev, Iorga-Sciusev, GiveWay);\n" +
                "SetMovementType(Sciusev, Unilateral);\n" +
                "SetLongtitudinalMarking(Sciusev, Broken);\n" +
                "\n" +
                "INTERSECTION Iorga-Sciusev;\n" +
                "SetIntersectionType(Iorga-Sciusev, Regulated);\n" +
                "SetCrossingAngle(Sciusev, 130);\n" +
                "\n" +
                "AutoMarkup();\n" +
                "Draw();\n", appData).parse());*/

        System.out.println(new RoadConstructionTestParser("ROAD a;\n" + "Road b;\n", appData).parse());

        HandleScriptEditor();

        stage.setTitle("Road Markup DSL");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void HandleScriptEditor() {
        testScriptButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                root.getChildren().removeIf(el -> el != scriptTextField && el != testScriptButton);
                _stageController = new StageController();

                String script = scriptTextField.getText();
                appData = new AppData();
                System.out.println(new RoadConstructionTestParser(script, appData).parse());
                new RoadConstructionTestParser(script, appData).parse();
                System.err.println(scriptTextField.getText());
                System.err.println("RUN SCRIPTS FROM HERE!");
                HandleRoads();

                for (RoadVX roadVX: _stageController.GetRoadsVX()) {
                    System.out.println(roadVX.roadVX);
                    root.getChildren().add(roadVX.roadVX);
                    root.getChildren().add(roadVX.markupVX.MarkupGroup);
                }

                for (SignVX signVX: _stageController.GetSignsVX()) {
                    System.out.println(signVX.signVX);
                    root.getChildren().add(signVX.signVX);
                }
            }
        });
    }

    private void HandleRoads() {
        String keys[] = new String[appData.roadHashMap.keySet().size()];
        Set<String> keySet = appData.roadHashMap.keySet();
        keySet.toArray(keys);

        for (int i = 0; i < keys.length; i++) {

            var r = appData.roadHashMap.get(keys[i]);
            var rVX = new RoadVX(r);
            _stageController.addRoadVXToList(rVX);
            HandleRoadsSigns(r, rVX);


        }
    }

    private void HandleRoadsSigns(Road road, RoadVX roadVX) {
        var keys = new String[road.signHashMap.keySet().size()];
        var keySet = road.signHashMap.keySet();
        keySet.toArray(keys);
        System.out.println("SINGSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
        System.out.println("SIZE " + road.signHashMap.keySet().size());
        for (int i = 0; i < keys.length; i++) {
            System.out.println("key: " + keys[i]);

            Sign s = road.signHashMap.get(keys[i]);
            System.out.println(s);

            _stageController.addSignVXToList(new SignVX(s, roadVX));
        }
    }
}

