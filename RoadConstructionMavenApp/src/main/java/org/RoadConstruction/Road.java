package org.RoadConstruction;

import org.DataProcessing.EventParser;

import java.util.HashMap;

public class Road {
    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    private String varName;

    public HashMap<String, Sign> signHashMap = new HashMap<>();

    private int width = 5;
    private int length = 30;
    private int angle = 0;

    public Road(){
    }

    public Road(String input){
        this.varName = EventParser.getInputArguments(input)[0];
    }

    public Road(int width){
        this.width = width;
    }

    public HashMap<String, Sign> getSigns() {return this.signHashMap;}

    //public void setSign (int index, Sign sign) {this.signHashMap.set(index, sign);}

    //public void setSigns (ArrayList<Sign> signs) {this.signHashMap = signs;}

    public void addSign (Sign sign) {this.signHashMap.put(sign.getVarName(), sign);}

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth(){
        return this.width;
    }

    public void setWidth(int width){
        this.width = width;
    }

    public String getVarName(){
        return varName;
    }

    @Override
    public String toString(){
        return this.varName + " " + this.getLength() + " " + this.getWidth() + " " + this.getAngle();
    }
}
