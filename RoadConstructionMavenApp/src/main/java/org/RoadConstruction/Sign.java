package org.RoadConstruction;

import org.DataProcessing.EventParser;

import java.util.HashMap;
import java.util.StringJoiner;

public class Sign {

    public enum SignType {
        STOP,
        ROUNDABOUT,
        GIVEWAY,
        NO_WAITING,
        DONT_STOP,
        MAIN,
        CROSSWALK,
        NOENTRY,
        ONEWAY,
        NOTURNLEFT,
        NOTURNAROUND,
        BAD_SIGN
    }



    public enum SignSide {
        LEFT,
        RIGHT,
        BAD_SIDE
    }

    private final String varName;

    private Road road;
    //what road belongs it.

    private int offset;
    //distance from the beginning of the road

    private SignSide sideOfRoad;
    //on which side of the road it is located

    private SignType type ;
    //type of sign itself

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public SignSide getSideOfRoad() {
        return sideOfRoad;
    }

    public Sign(String input, HashMap<String, Road> roadHashMap, HashMap<String, Sign> signHashMap) {
        System.out.println("QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ " + EventParser.getInputArguments(input)[0]);
        String roadName = EventParser.getInputArguments(input)[0];
        Road road = roadHashMap.get(roadName);

        this.varName = EventParser.getVarNameFromConstructorExpression(input);
        this.road = road;
        setType(EventParser.getInputArguments(input)[1]);
        this.offset = Integer.parseInt(EventParser.getInputArguments(input)[2]);
        setSide(EventParser.getInputArguments(input)[3]);

        signHashMap.put(this.varName, this);

        System.out.println(this);

        if (road != null)
            road.addSign(this);
        else
            System.out.println("Bad road name"); //trigger
    }


    public void setRoad(Road road) {
        this.road = road;
    }

    public void setDistance(int distance) {
        this.offset = distance;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Sign.class.getSimpleName() + "[", "]")
                .add("varName='" + varName + "'")
                .add("road=" + road)
                .add("offset=" + offset)
                .add("sideOfRoad='" + sideOfRoad + "'")
                .add("type='" + type + "'")
                .toString();
    }


    private void setType(String signType) {
        System.out.println("-----" + signType);
        for (SignType enumType: SignType.values()) {
            System.out.println("--" + enumType);
            if (enumType.name().equalsIgnoreCase(signType)) {
                this.type = enumType;
                return;
            }
        }
        //there have to be an error about wrong sign type.

        this.type = SignType.BAD_SIGN;
        System.out.println("THERE IS NO SUCH SIGN");
        // trigger
    }

    private void setSide(String signSide) {
        for (SignSide enumSide: SignSide.values()) {
            if (enumSide.name().equalsIgnoreCase(signSide)) {
                this.sideOfRoad = enumSide;
                return;
            }
        }
        //there have to be an error about wrong sign type.

        this.sideOfRoad = SignSide.BAD_SIDE;
        System.out.println("THERE IS NO SUCH SIGN");
    }

    public String getVarName() {
        return varName;
    }

    public SignType getType() {
        return type;
    }

    public Road getRoad() {
        return road;
    }
}


