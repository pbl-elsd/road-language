package org.VX.Elements;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.Main.App;

public class MarkupVX {
    double markupWidth = 5;
    double markupHeight = 1;
    public Group MarkupGroup = new Group();

    public MarkupVX(RoadVX roadVX) {

        System.out.println(roadVX.roadVX.getWidth()*roadVX.scale);
        System.out.println(roadVX.roadVX.getWidth()*roadVX.scale / 2 - (roadVX.roadVX.getHeight()*roadVX.scale + 20));
        for (int i = 10; i < roadVX.roadVX.getWidth()*roadVX.scale; i += 20)
        {
            if (i < roadVX.roadVX.getWidth()*roadVX.scale / 2 - (roadVX.roadVX.getHeight()*roadVX.scale) ||
                    i  > roadVX.roadVX.getWidth()*roadVX.scale/2 + roadVX.roadVX.getHeight()*roadVX.scale) {
                var rect = new Rectangle();
                rect.setWidth(10);
                rect.setHeight(5);

                rect.setX(roadVX.rescaledXY.x + i);
                rect.setY(App.Instance.center.y - markupHeight / 2);
                rect.setFill(Color.WHITE);

                MarkupGroup.getChildren().add(rect);
            }
        }
        var rect1 = new Rectangle();
        rect1.setWidth(50);
        rect1.setHeight(5);

        rect1.setX(roadVX.rescaledXY.x + roadVX.roadVX.getWidth()*roadVX.scale / 2
                - (roadVX.roadVX.getHeight()*roadVX.scale + 60));
        rect1.setY(App.Instance.center.y - markupHeight / 2);
        rect1.setFill(Color.WHITE);

        var rect2 = new Rectangle();
        rect2.setWidth(11);
        rect2.setHeight(roadVX.roadVX.getHeight() / 2 * 15);

        rect2.setX(roadVX.rescaledXY.x + roadVX.roadVX.getWidth()*roadVX.scale/2
                - (roadVX.roadVX.getHeight()*roadVX.scale + 10));
        rect2.setY(App.Instance.center.y - markupHeight / 2);
        rect2.setFill(Color.WHITE);

        var rect3 = new Rectangle();
        rect3.setWidth(50);
        rect3.setHeight(5);

        rect3.setX(roadVX.rescaledXY.x + roadVX.roadVX.getWidth()*roadVX.scale/2
                + (roadVX.roadVX.getHeight()*roadVX.scale));
        rect3.setY(App.Instance.center.y - markupHeight / 2);
        rect3.setFill(Color.WHITE);

        var rect4= new Rectangle();
        rect4.setWidth(11);
        rect4.setHeight(roadVX.roadVX.getHeight()/2*15);

        rect4.setX(roadVX.rescaledXY.x + roadVX.roadVX.getWidth()*roadVX.scale/2
                + (roadVX.roadVX.getHeight()*roadVX.scale));
        rect4.setY(App.Instance.center.y - roadVX.roadVX.getHeight()/2*roadVX.scale);
        rect4.setFill(Color.WHITE);

        MarkupGroup.getChildren().addAll(rect1, rect2, rect3, rect4);

        MarkupGroup.getTransforms().add(roadVX.rotate);

    }

}
