package org.VX.Elements;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import org.RoadConstruction.Road;
import org.Main.App;
import org.VX.Vector2;

public class RoadVX extends ElementVX {

    public Rectangle roadVX;

    public Vector2 rescaledXY = new Vector2();
    //public double rotated;
    public Rotate rotate = new Rotate();
    public double scale = 15;

    public MarkupVX markupVX;

    public RoadVX(Road road) {
        roadVX = new Rectangle();

        roadVX.setWidth(road.getLength());
        roadVX.setHeight(road.getWidth());

        roadVX.setX(App.Instance.center.x - roadVX.getWidth() / 2);
        roadVX.setY(App.Instance.center.y - roadVX.getHeight() / 2);



        rotate.setPivotX(App.Instance.center.x);
        rotate.setPivotY(App.Instance.center.y);

        rotate.setAngle(road.getAngle());
        //rotated = road.getAngle();

        roadVX.getTransforms().add(rotate);
        roadVX.setFill(Color.GRAY);

        roadVX.setScaleX(scale);
        roadVX.setScaleY(scale);

        rescaledXY.x = App.Instance.center.x - roadVX.getWidth() / 2 * scale;
        rescaledXY.y = App.Instance.center.y;

//        Circle circle = new Circle();
//        circle.setCenterX(rescaledXY.x);
//        circle.setCenterY(rescaledXY.y);
//        circle.setRadius(5);
//        circle.setFill(Color.BLACK);
//
//        App.Instance.root.getChildren().add(circle);
//        circle.getTransforms().add(rotate);
        markupVX = new MarkupVX(this);

    }

}
