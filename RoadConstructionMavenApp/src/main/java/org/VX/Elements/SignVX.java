package org.VX.Elements;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.RoadConstruction.Sign;
import org.Main.App;
import org.VX.Vector2;

public class SignVX {

    public ImageView signVX;

    public SignVX(Sign sign, RoadVX roadVX) {
        String signsPath = "file:src\\main\\java\\org\\VX\\Elements\\Sprites\\Signs\\";
        Image signImage = new Image(signsPath + "WarningSign_Template.png");

        switch (sign.getType())
        {
            case STOP:
                signImage = new Image(signsPath + "StopAhead.png");
                break;
            case GIVEWAY:
                signImage = new Image(signsPath + "GiveWay.png");
                break;
            case MAIN:
                signImage = new Image(signsPath + "MainRoad.png");
                break;
            case CROSSWALK:
                signImage = new Image(signsPath + "Crosswalk.png");
                break;
            case ROUNDABOUT:
                signImage = new Image(signsPath + "Roundabout.png");
                break;
            case NOENTRY:
                signImage = new Image(signsPath + "NoEntry.png");
                break;
            case ONEWAY:
                signImage = new Image(signsPath + "Oneway.png");
                break;
            case NOTURNLEFT:
                signImage = new Image(signsPath + "NoTurnLeft.png");
                break;
            case NOTURNAROUND:
                signImage = new Image(signsPath + "NoTurnAround.png");
                break;
            case BAD_SIGN:
                System.out.println("BAD SIGN PROVIDED");
                break;
        }

        signVX = new ImageView(signImage);

        var centerImage = GetCenterOfImage(signImage);

        if (sign.getSideOfRoad() == Sign.SignSide.RIGHT)
        {
            signVX.setX(roadVX.rescaledXY.x - centerImage.x + sign.getOffset() * 15);
            signVX.setY(roadVX.rescaledXY.y - centerImage.y + roadVX.roadVX.getHeight()  * 15);
        }
        else
        {
            signVX.setX(roadVX.rescaledXY.x - centerImage.x + sign.getOffset() * 15 * 2);
            signVX.setY(App.Instance.center.y - centerImage.y - roadVX.roadVX.getHeight()  * 15);
        }

        signVX.getTransforms().add(roadVX.rotate);

//        if (sign.getSideOfRoad() == Sign.SignSide.LEFT)
//            signVX.setRotate(90);
//        else if (sign.getSideOfRoad() == Sign.SignSide.RIGHT)
//            signVX.setRotate(-90);

//        Scale scale = new Scale();
//
//        scale.setX(0.15);
//        scale.setY(0.15);
//
//        scale.setPivotX(signVX.getX());
//        scale.setPivotY(signVX.getY());
//
//        signVX.getTransforms().add(scale);







    }

    private Vector2 GetCenterOfImage(Image img)
    {
        return new Vector2(img.getWidth() / 2, img.getHeight() / 2);
    }


}
