package org.VX;

import org.VX.Elements.RoadVX;
import org.VX.Elements.SignVX;

import java.util.ArrayList;

public class StageController {

    private ArrayList<RoadVX> RoadsVX;
    private ArrayList<SignVX> SignsVX;

    public StageController() {
        RoadsVX = new ArrayList<RoadVX>();
        SignsVX = new ArrayList<SignVX>();
    }

    public void addRoadVXToList(RoadVX _roadVX)
    {
        RoadsVX.add(_roadVX);
    }

    public void addSignVXToList(SignVX _signVX)
    {
        SignsVX.add(_signVX);
    }

    public ArrayList<RoadVX> GetRoadsVX()
    {
        return RoadsVX;
    }

    public ArrayList<SignVX> GetSignsVX()
    {
        return SignsVX;
    }
}
