package org.VX;

public class Vector2 {
    // Members
    public double x;
    public double y;

    // Constructors
    public Vector2() {
        this.x = 0.0d;
        this.y = 0.0d;
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Compare two vectors
    public boolean equals(Vector2 other) {
        return (this.x == other.x && this.y == other.y);
    }

    public static double distance(Vector2 a, Vector2 b) {
        double v0 = b.x - a.x;
        double v1 = b.y - a.y;
        return Math.sqrt(v0*v0 + v1*v1);
    }
    
    public void normalize() {
        // sets length to 1
        //
        double length = Math.sqrt(x*x + y*y);

        if (length != 0.0) {
            double s = 1.0f / (double)length;
            x = x*s;
            y = y*s;
        }
    }


}
